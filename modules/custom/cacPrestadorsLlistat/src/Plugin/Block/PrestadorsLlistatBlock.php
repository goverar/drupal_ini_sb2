<?php

namespace Drupal\cacPrestadorsLlistat\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Defines a custom block type.
 *
 * @Block(
 *  id = "cacPrestadorsLlistat",
 *  admin_label = @Translation("CAC Prestadors Llistat"),
 * )
 */
class PrestadorsLlistatBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'pagina_bloc_prestadors_llistat',
      '#attached' => [
        	'library' => [
          		'cacPrestadorsLlistat/prestadors',
        	]
      	]
    ];
  }

}
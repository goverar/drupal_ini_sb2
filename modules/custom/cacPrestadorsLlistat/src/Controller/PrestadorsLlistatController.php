<?php
// cacPrestadorsLlistat/src/Controller/PrestadorsLlistatController.php
namespace Drupal\cacPrestadorsLlistat\Controller;
 
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization\Json;
use Drupal;
 
class PrestadorsLlistatController extends ControllerBase {

    public function prestadorsLlistat() {
        $dades = $this->_get_prestadors();
        \Drupal::logger('cacPrestadorsLlistat')->notice('Resultat cerca Prestadors: ' . count($dades));
        $form = $this->formBuilder()->getForm('Drupal\cacPrestadorsLlistat\Form\PrestadorsLlistatForm');

        $ret = array ('#theme' => 'pagina_prestadors_llistat',
            		'#titol' => 'CERCADOR DE PRESTADORS',
            		'#descripcio' => 'Llistat de prestadors del CAC',
            		'#dades' => $dades,
            		'#formulari' => $form);

		\Drupal::logger('cacPrestadorsLlistat')->notice('Passa 4: Retorn de theme = ' . count($ret));        

        return $ret;
    }

	/**
 	* Get the element as object
 	* @param $uri the URI to get
 	* @return object
 	*/
	function _get_prestadors() {
        // Get the node with nid 31 using JSON format.
		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestadors: Passa 1.');
		$uri = "http://wsregpres.cac.cat/ws_regpres/api/llistaprestadors";
		// $uri = "https://jsonplaceholder.typicode.com/posts/1";

		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestadors: Passa 2.');
		$response = file_get_contents($uri);

		// $str = str_replace("@", '', $response);
		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestadors: Passa 3 : ' . $response);
		// $str = str_replace('\\"', '"', $str);
		// \Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestadors: Passa \" : ' . $str);

		$str = str_replace("<string>", '', $response);
		$str = str_replace("</string>", '', $str);
		$response = $str;
		$response = "[{\"idPrestador\":219,\"nif\":\"A15522535 \",\"denominacioSocial\":\"Agrupación Radiofónica,  SA (AGRURASA)\",\"Serveis\":2,\"tipusPersonalitat\":\"Jurídica\"},{\"idPrestador\":19,\"nif\":\"P0800100J \",\"denominacioSocial\":\"Ajuntament d\u0027Abrera\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":213,\"nif\":\"P2500300E \",\"denominacioSocial\":\"Ajuntament d\u0027Agramunt\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":521,\"nif\":\"P4300100G \",\"denominacioSocial\":\"Ajuntament d\u0027Aiguamúrcia\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":195,\"nif\":\"P2500800D \",\"denominacioSocial\":\"Ajuntament d\u0027Albesa\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":152,\"nif\":\"P4300400A \",\"denominacioSocial\":\"Ajuntament d\u0027Alcanar\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":423,\"nif\":\"P2501200F \",\"denominacioSocial\":\"Ajuntament d\u0027Alcoletge\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"}]";

		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestadors: Passa 4 : ' . $response);
		// Drupal 7: return drupal_json_decode($response);
		// Drupal 8: return Json::decode($response);
		return Json::decode($response);
	}

	public function prestadorFitxa($id = 0) {
        $dades = $this->_get_prestador($id);
        \Drupal::logger('cacPrestadorsLlistat')->notice('Resultat Prestador: ' . count($dades));

        $ret = array ('#theme' => 'pagina_prestador_fitxa',
            		'#titol' => t('DADES DEL PRESTADOR'),
            		'#descripcio' => t('Dades públiques del prestador'),
            		'#dades' => $dades);

		\Drupal::logger('cacPrestadorsLlistat')->notice('Passa 4: Retorn de theme = ' . count($ret));        

        return $ret;
	}

	function _get_prestador($id = 0) {
		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestador ( '. $id . ' ): Get prestador');
		$ret = array();
        $ret[] = array('id' => 1, 'nom' => 'nom 1', 'pob' => 'Barcelona');
		\Drupal::logger('cacPrestadorsLlistat')->notice('_get_prestador ( '. $id . ' ): Fi Get prestador');

		return $ret;
	}

    public function personesLlistat() {
    	\Drupal::logger('cacPrestadorsLlistat')->notice('Passa 1 - persones');

        // $form = $this->formBuilder()->getForm('Drupal\cacPrestadorsLlistat\Form\PrestadorsLlistatForm');

        $dades = array();
        $dades[] = array('id' => 1, 'nom' => 'nom 1', 'pob' => 'Barcelona');
        $dades[] = array('id' => 2, 'nom' => 'nom 2', 'pob' => 'Girona');
        $dades[] = array('id' => 3, 'nom' => 'nom 3', 'pob' => 'Lleida');
        $dades[] = array('id' => 4, 'nom' => 'nom 4', 'pob' => 'Tarragona');
        $dades[] = array('id' => 5, 'nom' => 'nom 5', 'pob' => 'Barcelona');
        $dades[] = array('id' => 6, 'nom' => 'nom 6', 'pob' => 'Girona');
        $dades[] = array('id' => 7, 'nom' => 'nom 7', 'pob' => 'Lleida');
        $dades[] = array('id' => 8, 'nom' => 'nom 8', 'pob' => 'Tarragona');        
        \Drupal::logger('cacPrestadorsLlistat')->notice('Passa 2 - persones: Registres trobats = ' . count($dades));

        $ret = array ('#theme' => 'pagina_persones_llistat',
            	'#titulo' => 'Llistat de Persones *** ',
            	'#descripcion' => 'Llistat de persones del CAC',
            	// '#formulario' => $form,
            	'#dades' => $dades);

		\Drupal::logger('cacPrestadorsLlistat')->notice('Passa 3 - persones: Retorn de theme = ' . count($ret));        

        return $ret;
    }	


}
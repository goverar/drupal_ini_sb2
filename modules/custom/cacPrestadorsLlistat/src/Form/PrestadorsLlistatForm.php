<?php
//cacPrestadorsLlistat/src/FormLlistatPrestadorsForm.php

namespace Drupal\cacPrestadorsLlistat\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Desarrollo de un formulario en drupal 8
 * @author Ignacio Farré
 */
class PrestadorsLlistatForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'prestadors_llistat_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        \Drupal::logger('cacPrestadorsLlistat')->notice('PrestadorsLlistatForm->buildForm() - 1 -');        

        $form['denominacioSocial'] = [
            '#type'     => 'textfield',
            '#title'    => $this->t('Nom / Denominació Social'),
            // '#description' => $this->t('Escriure text per cercar un prestador'),
            '#required' => FALSE
        ];
        $form['tipusPersonalitat'] = [
            '#type'     => 'select',
            '#title'    => $this->t('Tipus de prestador'),
            // '#description' => $this->t('Escollir tipus de prestador'),
            '#required' => FALSE,
            '#options' => array("-1" => t('Escollir tipus de prestador'), "1" => t('Públic'), "2" => t('Privat'))
        ];
        $form['nomServei'] = [
            '#type'          => 'textfield',
            '#title'         => $this->t('Nom del servei'),
            // '#default_value' => $this->t('Un gasto mas.'),
            '#required'      => FALSE
        ];        
        $form['accionista'] = [
            '#type'          => 'textfield',
            '#title'         => $this->t('Accionista'),
            // '#default_value' => $this->t('Un gasto mas.'),
            '#required'      => FALSE
        ];                
        $form['submit'] = [
            '#type'  => 'submit',
            '#value' => $this->t('Consulta'),
        ];
        \Drupal::logger('cacPrestadorsLlistat')->notice('PrestadorsLlistatForm->buildForm() - 2 -');        
        // return parent::buildForm($form, $form_state);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        /*
        if ($form_state->getValue('cantidad') < 1) {
            $form_state->setErrorByName('cantidad', $this->t('La cantidad tiene que ser mayor que 1'));
        }
        */
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Display result.
        /*
        foreach ($form_state->getValues() as $key => $value) {
            drupal_set_message($key . ': ' . $value);
        }
        */
    }
}
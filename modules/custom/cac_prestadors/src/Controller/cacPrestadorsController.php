<?php

/**
 * @file
 * Contains \Drupal\cac_prestadors\Controller\cacPrestadorsController.
 */

namespace Drupal\cac_prestadors\Controller;

use Drupal;
/*use Drupal\Component\Render\FormattableMarkup;*/
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

class cacPrestadorsController extends ControllerBase
{

    /**
     * Display the markup.
     *
     * @return array
     */

    public function prestadorsLlistat()
    {

        // get the data from ws in array
        $dades = $this->_get_prestadors();

        $header = [
            // We make it sortable by name.
            ['data' => $this->t('Nom / Denominació Social'), 'field' => 'denominacioSocial', 'sort' => 'asc'],
            ['data' => $this->t('Personalitat')],
            ['data' => $this->t('NIF/CIF')],
            ['data' => $this->t('Serveis')],
            ['data' => $this->t('+Info')],
        ];

        $rows = [];
        foreach ($dades as $dade) {
            $rows[] = array(
                //'tipusPersonalitat' => $dade->access('view') ? $node->getTitle() : t('Redacted'),
                'denominacioSocial' => $dade['denominacioSocial'],
                'tipusPersonalitat' => $dade['tipusPersonalitat'],
                'nif'               => $dade['nif'],
                'Serveis'           => $dade['Serveis'],
                'info'              => Link::createFromRoute($this->t('dades'), 'cac_prestadors_llistat.info.tab1', ['id' => $dade['idPrestador']]),

                /*'idPrestador' => $dade['idPrestador'],
            array('data' => new FormattableMarkup('<a href=":link">@name</a>',
            [':link' => '/prestadors/registre-prestadors/info/' . $dade['idPrestador'],
            '@name'  => 'dades']),
            ),*/

            );
        }

        \Drupal::logger('cac_prestadors')->notice('Resultat cerca Prestadors: ' . count($dades));
        $form = \Drupal::formBuilder()->getForm('Drupal\cac_prestadors\Form\cacPrestadorsForm');

        /*$form = $this->formBuilder()->getForm('Drupal\cac_prestadors\Form\cacPrestadorsForm');*/

        $build = [
            '#theme'      => 'pagina_prestadors_llistat',
            '#titol'      => $this->t("PRESTADORS QUE FORMEN PART DEL 'REGISTRE DE PRESTADORS' DEL CONSELL DE L'AUDIOVISUAL DE CATALUNYA"),
            '#descripcio' => 'Llistat de prestadors del CAC',
            '#dades'      => $dades,
            '#formulari'  => $form,
            '#attached'   => [
                'library' => [
                    'cac_prestadors/cac',
                ],
            ],
        ];

        /*'#attributes' => ['class' => ['aggregator-wrapper']],*/

        /*$build = ['description' => [
        '#theme'       => 'pagina_prestadors_llistat',
        '#description' => 'foo',
        '#attributes'  => [],
        ],
        ];*/

        // Initialize the pager
        $num_per_page = 20;
        $current_page = pager_default_initialize(count($rows), $num_per_page);

        // Split your list into page sized chunks
        $chunks = array_chunk($rows, $num_per_page, true);

        // Build a render array which will be themed as a table with a pager.
        $build['pager_example'] = array(
            '#rows'   => $chunks[$current_page],
            '#header' => $header,
            '#type'   => 'table',
            '#empty'  => t('No content available.'),
        );

        $build['pager'] = array(
            '#type'     => 'pager',
            '#weight'   => 10,
            '#quantity' => count($rows),
        );

        \Drupal::logger('cac_prestadors')->notice('Passa 4: Retorn de theme = ' . count($ret));

        return $build;
    }

    public function _get_prestadors()
    {
        // Get the node with nid 31 using JSON format.
        \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 1.');
        $uri = "http://wsregpres.cac.cat/ws_regpres/api/llistaprestadors";
        // $uri = "https://jsonplaceholder.typicode.com/posts/1";

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 2.');
        $response = file_get_contents($uri);

        // $str = str_replace("@", '', $response);
        \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 3 : ' . $response);
        // $str = str_replace('\\"', '"', $str);
        // \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa \" : ' . $str);

        $str      = str_replace("<string>", '', $response);
        $str      = str_replace("</string>", '', $str);
        $response = $str;

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 4 : ' . $response);
        // Drupal 7: return drupal_json_decode($response);
        // Drupal 8: return Json::decode($response);
        return Json::decode($response);
    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorInfo($id = 0)
    {

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/infoPrestador/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_inf = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/participacions/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_par = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/decisions/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_dec = Json::decode($response);

        $uri       = "http://wsregpres.cac.cat/ws_regpres/api/organsgovern/id/" . $id;
        $response  = file_get_contents($uri);
        $dades_org = Json::decode($response);

        $uri         = "http://wsregpres.cac.cat/ws_regpres/api/llistaserveisprestador/id/" . $id;
        $response    = file_get_contents($uri);
        $dades_llist = Json::decode($response);

        $ret = ['#theme'  => 'pagina_prestador_info',
            '#titol_inf'      => t('DADES DEL PRESTADOR'),
            '#descripcio_inf' => t('Dades públiques del prestador'),
            '#dades_inf'      => $dades_inf,
            '#titol_par'         => t('DADES DE PARTICIPACIÓ DEL PRESTADOR'),
            '#descripcio_par'    => t('Dades públiques del prestador'),
            '#dades_par'      => $dades_par,
            '#titol_dec'         => t('DADES DECISIONS DEL PRESTADOR'),
            '#descripcio_dec'    => t('Dades públiques del prestador'),
            '#dades_dec'      => $dades_dec,
            '#titol_org'         => t('DADES GOVERN DEL PRESTADOR'),
            '#descripcio_org'    => t('Dades públiques del prestador'),
            '#dades_org'      => $dades_org,
            '#titol_llist'         => t('LLISTA SERVEIS DEL PRESTADOR'),
            '#descripcio_llist'    => t('Dades públiques del prestador'),
            '#dades_llist'    => $dades_llist,
        ];

        return $ret;

    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorFitxa($id = 0)
    {

        $dades = $this->_get_prestador_fitxa($id);
        \Drupal::logger('cac_prestadors')->notice('Resultat Prestador: ' . count($dades));

        $ret = ['#theme' => 'pagina_prestador_fitxa',
            '#titol'         => t('DADES DEL PRESTADOR'),
            '#descripcio'    => t('Dades públiques del prestador'),
            '#dades'         => $dades,
        ];

        \Drupal::logger('cac_prestadors')->notice('Passa 4: Retorn de theme = ' . count($ret));

        return $ret;

    }

    public function _get_prestador_fitxa($id = 0)
    {

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 1.');
        $uri = "http://wsregpres.cac.cat/ws_regpres/api/infoPrestador/id/" . $id;

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 2.');
        $response = file_get_contents($uri);

        // $str = str_replace("@", '', $response);
        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 3 : ' . $response);

        // $str = str_replace('\\"', '"', $str);
        // \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa \" : ' . $str);

        // Drupal 7: return drupal_json_decode($response);
        // Drupal 8: return Json::decode($response);
        return Json::decode($response);

    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorParticipacions($id = 0)
    {

        $uri = "http://wsregpres.cac.cat/ws_regpres/api/participacions/id/" . $id;

        $response = file_get_contents($uri);

        $dades = Json::decode($response);

        $ret = ['#theme' => 'pagina_prestador_participacions',
            '#titol'         => t('DADES DE PARTICIPACIÓ DEL PRESTADOR'),
            '#descripcio'    => t('Dades públiques del prestador'),
            '#dades'         => $dades,
        ];

        return $ret;

    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorDecisions($id = 0)
    {

        $uri = "http://wsregpres.cac.cat/ws_regpres/api/decisions/id/" . $id;

        $response = file_get_contents($uri);

        $dades = Json::decode($response);

        $ret = ['#theme' => 'pagina_prestador_decisions',
            '#titol'         => t('DADES DECISIONS DEL PRESTADOR'),
            '#descripcio'    => t('Dades públiques del prestador'),
            '#dades'         => $dades,
        ];

        return $ret;

    }

    /**
     * Display the markup.
     *
     * @return array
     */
    public function prestadorGovern($id = 0)
    {

        $uri = "http://wsregpres.cac.cat/ws_regpres/api/organsgovern/id/" . $id;

        $response = file_get_contents($uri);

        $dades = Json::decode($response);

        $ret = ['#theme' => 'pagina_prestador_govern',
            '#titol'         => t('DADES GOVERN DEL PRESTADOR'),
            '#descripcio'    => t('Dades públiques del prestador'),
            '#dades'         => $dades,
        ];

        return $ret;

    }

    public function prestadorServeis($id = 0)
    {

        $uri = "http://wsregpres.cac.cat/ws_regpres/api/llistaserveisprestador/id/" . $id;

        $response = file_get_contents($uri);

        $dades = Json::decode($response);

        $ret = ['#theme' => 'pagina_prestador_serveis',
            '#titol'         => t('LLISTA SERVEIS DEL PRESTADOR'),
            '#descripcio'    => t('Dades públiques del prestador'),
            '#dades'         => $dades,
        ];

        return $ret;

    }

    /*public function personesLlistat() {
\Drupal::logger('cac_prestadors')->notice('Passa 1 - persones');

// $form = $this->formBuilder()->getForm('Drupal\cac_prestadors\Form\PrestadorsLlistatForm');

$dades = array();
$dades[] = array('id' => 1, 'nom' => 'nom 1', 'pob' => 'Barcelona');
$dades[] = array('id' => 2, 'nom' => 'nom 2', 'pob' => 'Girona');
$dades[] = array('id' => 3, 'nom' => 'nom 3', 'pob' => 'Lleida');
$dades[] = array('id' => 4, 'nom' => 'nom 4', 'pob' => 'Tarragona');
$dades[] = array('id' => 5, 'nom' => 'nom 5', 'pob' => 'Barcelona');
$dades[] = array('id' => 6, 'nom' => 'nom 6', 'pob' => 'Girona');
$dades[] = array('id' => 7, 'nom' => 'nom 7', 'pob' => 'Lleida');
$dades[] = array('id' => 8, 'nom' => 'nom 8', 'pob' => 'Tarragona');
\Drupal::logger('cac_prestadors')->notice('Passa 2 - persones: Registres trobats = ' . count($dades));

$ret = array ('#theme' => 'pagina_persones_llistat',
'#titulo' => 'Llistat de Persones *** ',
'#descripcion' => 'Llistat de persones del CAC',
// '#formulario' => $form,
'#dades' => $dades);

\Drupal::logger('cac_prestadors')->notice('Passa 3 - persones: Retorn de theme = ' . count($ret));

return $ret;
}    */

}

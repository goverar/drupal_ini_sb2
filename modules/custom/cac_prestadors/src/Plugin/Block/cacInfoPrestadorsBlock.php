<?php

namespace Drupal\cac_prestadors\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Json;
use Drupal;

/**
 * Defines a custom block type.
 *
 * @Block(
 *  id = "cacPrestadorsInfo",
 *  admin_label = @Translation("CAC Prestadors Info")
 * )
 */
class cacInfoPrestadorsBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build($id = 0) {
        $dades = $this->_get_prestador($id);
        \Drupal::logger('cac_prestadors')->notice('Resultat Prestador: ' . count($dades));

        $ret = array ('#theme' => 'pagina_prestador_fitxa',
                '#titol' => t('DADES DEL PRESTADOR'),
                '#descripcio' => t('Dades públiques del prestador'),
                '#dades' => $dades);

    \Drupal::logger('cac_prestadors')->notice('Passa 4: Retorn de theme = ' . count($ret));        

        return $ret;
  }

  function _get_prestador($id = 0) {
       
       \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 1.');
        $uri = "http://wsregpres.cac.cat/ws_regpres/api/infoPrestador/id/516";

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 2.');
        $response = file_get_contents($uri);

        // $str = str_replace("@", '', $response);
        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 3 : ' . $response);
        // $str = str_replace('\\"', '"', $str);
        // \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa \" : ' . $str);

        /*$response = "[{\"idPrestador\":219,\"nif\":\"A15522535 \",\"denominacioSocial\":\"Agrupación Radiofónica,  SA (AGRURASA)\",\"Serveis\":2,\"tipusPersonalitat\":\"Jurídica\"},{\"idPrestador\":19,\"nif\":\"P0800100J \",\"denominacioSocial\":\"Ajuntament d\u0027Abrera\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":213,\"nif\":\"P2500300E \",\"denominacioSocial\":\"Ajuntament d\u0027Agramunt\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":521,\"nif\":\"P4300100G \",\"denominacioSocial\":\"Ajuntament d\u0027Aiguamúrcia\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":195,\"nif\":\"P2500800D \",\"denominacioSocial\":\"Ajuntament d\u0027Albesa\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":152,\"nif\":\"P4300400A \",\"denominacioSocial\":\"Ajuntament d\u0027Alcanar\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":423,\"nif\":\"P2501200F \",\"denominacioSocial\":\"Ajuntament d\u0027Alcoletge\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"}]";*/

        \Drupal::logger('cac_prestadors')->notice('_get_prestadors_id: Passa 4 : ' . $response);
        // Drupal 7: return drupal_json_decode($response);
        // Drupal 8: return Json::decode($response);
        return Json::decode($response);

  }

}


<?php

namespace Drupal\cac_prestadors\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Json;
use Drupal;

/**
 * Defines a custom block type.
 *
 * @Block(
 *  id = "cacPrestadorsLlistat",
 *  admin_label = @Translation("CAC Prestadors Llistat")
 * )
 */
class cacPrestadorsBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

        $dades = $this->_get_prestadors();
        \Drupal::logger('cac_prestadors')->notice('Resultat cerca Prestadors: ' . count($dades));
        $form = \Drupal::formBuilder()->getForm('Drupal\cac_prestadors\Form\cacPrestadorsForm');

        $ret =  [ '#theme' => 'pagina_prestadors_llistat',
                  '#titol' => 'CERCADOR DE PRESTADORS',
                  '#descripcio' => 'Llistat de prestadors del CAC',
                  '#dades' => $dades,
                  '#formulari' => $form,
                  '#attached' => [
                    'library' => [
                      'cac_prestadors/cac',
                    ],
                  ],
                ];

        \Drupal::logger('cac_prestadors')->notice('Passa 4: Retorn de theme = ' . count($ret));        

        return $ret;
    }

    function _get_prestadors() {
      // Get the node with nid 31 using JSON format.
      \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 1.');
      $uri = "http://wsregpres.cac.cat/ws_regpres/api/llistaprestadors";
      // $uri = "https://jsonplaceholder.typicode.com/posts/1";

      \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 2.');
      $response = file_get_contents($uri);

      // $str = str_replace("@", '', $response);
      \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 3 : ' . $response);
      // $str = str_replace('\\"', '"', $str);
      // \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa \" : ' . $str);

      $str = str_replace("<string>", '', $response);
      $str = str_replace("</string>", '', $str);
      $response = $str;
      /*$response = "[{\"idPrestador\":219,\"nif\":\"A15522535 \",\"denominacioSocial\":\"Agrupación Radiofónica,  SA (AGRURASA)\",\"Serveis\":2,\"tipusPersonalitat\":\"Jurídica\"},{\"idPrestador\":19,\"nif\":\"P0800100J \",\"denominacioSocial\":\"Ajuntament d\u0027Abrera\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":213,\"nif\":\"P2500300E \",\"denominacioSocial\":\"Ajuntament d\u0027Agramunt\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":521,\"nif\":\"P4300100G \",\"denominacioSocial\":\"Ajuntament d\u0027Aiguamúrcia\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":195,\"nif\":\"P2500800D \",\"denominacioSocial\":\"Ajuntament d\u0027Albesa\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":152,\"nif\":\"P4300400A \",\"denominacioSocial\":\"Ajuntament d\u0027Alcanar\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"},{\"idPrestador\":423,\"nif\":\"P2501200F \",\"denominacioSocial\":\"Ajuntament d\u0027Alcoletge\",\"Serveis\":1,\"tipusPersonalitat\":\"Administració Pública\"}]";*/

      \Drupal::logger('cac_prestadors')->notice('_get_prestadors: Passa 4 : ' . $response);
      // Drupal 7: return drupal_json_decode($response);
      // Drupal 8: return Json::decode($response);
      return Json::decode($response);
    }

}


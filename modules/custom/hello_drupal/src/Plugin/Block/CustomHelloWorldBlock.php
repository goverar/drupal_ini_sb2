<?php

namespace Drupal\hello_drupal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
/** 
 * Definición de nuestro bloque
 *
 * @Block(
 *   id = "hello_drupal",
 *   admin_label = @Translation("block hello word")
 * )
 */

class CustomHelloWorldBlock extends BlockBase {
	/**
	 * {@inheritdoc}
	 */
  	 
  	//codigo retorna en twig
  	public function build() {
        //Aquí podemos acceder a la base de datos y controlar datos para enviarlos a nuestro bloque ;)
        return [
            '#theme' => 'show_hello_drupal',
            '#titulo' => 'Mi titulo hello world',
            '#descripcion' => 'página twig from block drupal y solo puede ser visualizado por menu/why-do-we-use-it'
        ];
    }


/*
    // código para retornar directo a la página
  	public function build() {
        return [
            '#type' => 'markup',
            '#markup' => 'mi bloque por fin!!!!!!!!',
        ];
    }*/

}